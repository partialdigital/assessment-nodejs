const { getServerResponses } = require('./getServerResponses');

module.exports.findServer = async () => {
  try {
    const responses = await getServerResponses();

    // Filter out offline servers
    const onlineServers = responses.filter((response) => response.online);
    const numOnlineServers = onlineServers.length;

    // Throw error if no servers online
    if (numOnlineServers < 1) {
      throw new Error('All servers offline.');
    }

    // Sort servers on priority
    onlineServers.sort((a, b) => a.priority - b.priority);
    return onlineServers[0];
  } catch (error) {
    return error.message;
  }
};
