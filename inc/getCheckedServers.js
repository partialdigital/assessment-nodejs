require('dotenv').config();
const { readStaticFile } = require('./readStaticFile');
const { checkServer } = require('./checkServer.js');

module.exports.getCheckedServers = async () => readStaticFile(process.env.PATH_TO_SERVERS)
  .then((data) => {
    const servers = JSON.parse(data);
    return servers.map(({ port }) => new Promise((resolve) => checkServer(port)
      .then((response) => resolve(response.data))
      .catch(() => resolve({ online: 0 }))));
  });
