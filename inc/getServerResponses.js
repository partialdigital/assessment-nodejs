const { getCheckedServers } = require('./getCheckedServers');

module.exports.getServerResponses = async () => {
  const checkedServers = await getCheckedServers();
  return Promise.all(checkedServers)
    .then((responses) => responses)
    .catch((error) => error);
};
