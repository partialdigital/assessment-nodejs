/* global it expect */
const { checkServer } = require('../inc/checkServer.js');

it('tests if server is running', async () => {
  expect.assertions(1);
  try {
    const { status } = await checkServer(3001);
    expect(status).toBe(200);
  } catch (e) {
    expect(e).toEqual(
      expect.objectContaining({
        // check for ECONNREFUSED or ECONNRESET
        code: expect.stringMatching('ECONNRE'),
      }),
    );
  }
});
