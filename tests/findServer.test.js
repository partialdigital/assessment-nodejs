/* global it expect */
const { findServer } = require('../inc/findServer.js');
const { getServerResponses } = require('../inc/getServerResponses.js');

const getLowestPriority = async () => {
  const serverResponses = await getServerResponses();
  const priorityValues = serverResponses
    .filter(({ priority }) => typeof priority !== 'undefined')
    .map(({ priority }) => priority);
  return Math.min(...priorityValues);
};

it('tests if returned server has lowest priority', async () => {
  expect.assertions(1);
  try {
    const { priority } = await findServer();
    const lowestPriority = await getLowestPriority();
    expect(priority).toBe(lowestPriority);
  } catch (e) {
    expect(e).toEqual({
      error: 'All servers offline.',
    });
  }
});
