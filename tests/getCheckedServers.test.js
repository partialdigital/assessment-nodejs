/* global test expect */
const { getCheckedServers } = require('../inc/getCheckedServers.js');

test('data is an array', async () => {
  const data = await getCheckedServers();
  expect(data).toBeInstanceOf(Array);
});

test('data has length of 4', async () => {
  const data = await getCheckedServers();
  expect(data).toHaveLength(4);
});
