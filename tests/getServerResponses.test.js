/* global test expect */
const { getServerResponses } = require('../inc/getServerResponses.js');

test('object in array has online attribute', async () => {
  const data = await getServerResponses();
  expect(data).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        online: expect.any(Number),
      }),
    ]),
  );
});
