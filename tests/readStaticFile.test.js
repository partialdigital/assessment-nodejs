/* global test expect */
require('dotenv').config();
const { readStaticFile } = require('../inc/readStaticFile.js');

test('data is resolving promise', async () => {
  const path = process.env.PATH_TO_SERVERS;
  await expect(readStaticFile(path)).resolves;
});
